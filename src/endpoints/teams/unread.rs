#[derive(Debug, Deserialize)]
pub struct TeamUnread {
    team_id: String,
    msg_count: i64,
    mention_count: i64,
}
