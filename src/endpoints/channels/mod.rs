use endpoints::common::NotifyProps;

use std::collections::HashMap;

#[derive(Debug, Deserialize)]
pub struct Channel {
    pub id: String,
    pub create_at: i64,
    pub update_at: i64,
    pub delete_at: i64,
    pub team_id: String,
    pub team_type: String,
    pub display_name: String,
    pub name: String,
    pub header: String,
    pub purpose: String,
    pub last_post_at: String,
    pub total_msg_count: String,
    pub extra_update_at: String,
    pub creator_id: String,
    pub scheme_id: String,
    pub props: HashMap<String, String>,
}

#[derive(Debug, Deserialize)]
pub struct Channels(pub Vec<Channel>);

#[derive(Debug, Deserialize)]
pub struct ChannelUnread {
    team_id: String,
    channel_id: String,
    msg_count: i64,
    mention_count: i64,
    notify_props: NotifyProps,
}
