#[derive(Debug, Deserialize)]
pub struct NotifyProps {
    pub channel: String,
    pub comments: String,
    pub desktop: String,
    pub desktop_sound: String,
    pub email: String,
    pub first_name: String,
    pub mention_keys: String,
    pub push: String,
    pub push_status: String,
}
