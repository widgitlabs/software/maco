use endpoints::common::NotifyProps;
use endpoints::common::Timezone;

#[derive(Debug, Deserialize)]
pub struct User {
    pub id: String,
    pub create_at: Option<i64>,
    pub update_at: Option<i64>,
    pub delete_at: i64,
    pub username: String,
    pub auth_data: Option<String>,
    pub auth_service: String,
    pub email: String,
    pub email_verified: Option<bool>,
    pub nickname: String,
    pub first_name: String,
    pub last_name: String,
    pub position: String,
    pub roles: String,
    pub allow_marketing: Option<bool>,
    pub notify_props: Option<NotifyProps>,
    pub last_password_update: Option<i64>,
    pub last_picture_update: Option<i64>,
    pub locale: String,
    pub timezone: Option<Timezone>,
}
