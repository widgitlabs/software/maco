use reqwest::{self, StatusCode};
use serde_json;

#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "connection error: {}", why)]
    Connection { why: reqwest::Error },
    #[fail(display = "failed to deserialize json response: {}", why)]
    Json { why: serde_json::Error },
    #[fail(display = "server response failed: status code {}", status)]
    HTTP { status: StatusCode },
    #[fail(display = "authentication token was not found")]
    TokenNotFound,
    #[fail(display = "authentication token was not UTF-8")]
    TokenInvalidUtf8,
}

impl From<reqwest::Error> for Error {
    fn from(why: reqwest::Error) -> Self {
        Error::Connection { why }
    }
}

impl From<serde_json::Error> for Error {
    fn from(why: serde_json::Error) -> Self {
        Error::Json { why }
    }
}
